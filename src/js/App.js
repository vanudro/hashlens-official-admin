const API_URL = "http://localhost:8080/";
const API_REMOTE = "http://admin.hashlens.nl:8080/";
const ROOT_URL = "http://localhost:3000/";
const ROOT_REMOTE = "admin.hashlens.nl/";

export class App {

    static getApiLink(fields) {
        if (window.location.href.indexOf("localhost") > -1) {
            return API_URL + fields;
        } else {
            return API_REMOTE + fields;
        }
    }

    static getLink(fields) {
        if (window.location.href.indexOf("localhost") > -1) {
            return ROOT_URL + fields;
        } else {
            return ROOT_REMOTE + fields;
        }
    }
}
import React from 'react';
import ReactDOM from 'react-dom';
import './css/master.css';
import * as serviceWorker from './serviceWorker';
import {MainRouter} from "./MainRouter";

ReactDOM.render(<MainRouter />, document.getElementById('root'));

// If you want your app_id to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

import React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {LoginPage} from "./components/pages/LoginPage";
import {DashboardPage} from "./components/pages/DashboardPage";
import {AccountPage} from "./components/pages/AccountPage";
import {ActivationPage} from "./components/pages/ActivationPage";
import {MenuBar} from "./components/MenuBar";
import {ChannelPage} from "./components/pages/ChannelPage";

const USER_KEY = "user";
const DATE_KEY = "date";

export class MainRouter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {login: false};
    }

    componentDidMount() {
        let expire_at = new Date(MainRouter.getDate());
        let date_now = new Date();

        // If date now is past expire date
        if (date_now.getTime() < expire_at.getTime()) {
            this.setState({login: MainRouter.isLoggedIn()});
        } else {
            console.log("Logout");
            MainRouter.logout(true);
        }
    }

    static addHoursToCurrentDate(h) {
        let now = new Date();
        now.setTime(now.getTime() + (h * 60 * 60 * 1000));
        return now;
    }

    static isLoggedIn() {
        return (sessionStorage.getItem(USER_KEY) != null);
    }

    static login(ID) {
        console.log("Logging in with ID: " + ID);
        if (sessionStorage.getItem(USER_KEY) == null) {
            sessionStorage.setItem(USER_KEY, ID);
            sessionStorage.setItem(DATE_KEY, MainRouter.addHoursToCurrentDate(6).toString());
            return true;
        }
        return false;
    }

    static getID() {
        return (sessionStorage.getItem(USER_KEY) != null)? sessionStorage.getItem(USER_KEY) : 0;
    }

    static getDate() {
        return (sessionStorage.getItem(DATE_KEY) != null)? sessionStorage.getItem(DATE_KEY) : new Date();
    }

    static logout(force = false) {
        console.log("Logging out");
        if (MainRouter.isLoggedIn() || force) {
            sessionStorage.removeItem(USER_KEY);
            sessionStorage.removeItem(DATE_KEY);
            return true;
        }
        return false;
    }

    static refresh(url, milliseconds = 1000) {
        setTimeout(function () {
            window.location = url;
        }, milliseconds);
    }

    render() {
        if (this.state.login) {
            return (
                <Router>
                    <Switch>
                        <Route path="/logout">
                            <LoginPage type={"logout"}/>
                        </Route>
                        <Route path="/account/password">
                            <AccountPage type="password"/>
                        </Route>
                        <Route path="/account/delete">
                            <AccountPage type="delete"/>
                        </Route>
                        <Route path="/account">
                            <AccountPage type="user"/>
                        </Route>
                        <Route path="/api/edit/:id">
                            <MenuBar/>
                            <ChannelPage/>
                        </Route>
                        <Route path="/api/authorize/:id">
                            <MenuBar/>
                            <ChannelPage type="authorize"/>
                        </Route>
                        <Route path="/api/delete/:id">
                            <MenuBar/>
                            <ChannelPage type="delete"/>
                        </Route>
                        <Route path="/api/add">
                            <MenuBar/>
                            <ChannelPage/>
                        </Route>
                        <Route path="/api/management">
                            <MenuBar/>
                            <ChannelPage type="management"/>
                        </Route>
                        <Route path="/">
                            <MenuBar/>
                            <DashboardPage/>
                        </Route>
                    </Switch>
                </Router>
            );
        } else {
            return (
                <Router>
                    <Switch>
                        <Route path="/activation">
                            <ActivationPage/>
                        </Route>
                        <Route path="/password-recover">
                            <LoginPage type={"recovery"}/>
                        </Route>
                        <Route path="/password-forget">
                            <LoginPage type={"forget"}/>
                        </Route>
                        <Route path="/register">
                            <LoginPage type={"register"}/>
                        </Route>
                        <Route path="/">
                            <LoginPage/>
                        </Route>
                    </Switch>
                </Router>
            );
        }
    }
}
import React from "react";
import {Avatar} from "./Avatar";

export class IconAvatar extends Avatar {

    constructor(props) {
        super(props);
        this.state = this.getState(this.props);
    }

    getState(props) {
        let state = super.getState(props);
        state.icon = (props.icon != null)? props.icon : null;
        return state;
    }

    componentDidUpdate(props) {
        this.setState(this.getState(props));
    }

    shouldComponentUpdate(props) {
        return (props.image !== this.state.image);
    }

    render() {
        return (
            <div className="avatar d-flex justify-content-center">
                <img src={this.state.image} alt={this.state.alt} className={"image shadow rounded"}/>
            </div>
        );
    }
}
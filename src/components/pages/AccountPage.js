import React from "react";
import {UserEditForm} from "../views/forms/UserEditForm";
import {PasswordChangeForm} from "../views/forms/PasswordChangeForm";
import {UserDeleteForm} from "../views/forms/UserDeleteForm";

export class AccountPage extends React.Component {

    constructor(props) {
        super(props);
        let type, form;
        type = (this.props.type != null)? this.props.type : null;
        switch (type) {
            case "password":
                form = (<PasswordChangeForm/>);
                break;
            case "delete":
                form = (<UserDeleteForm/>);
                break;
            default:
                form = (<UserEditForm/>);
                break;
        }
        this.state = {form: form};
    }

    render() {
        return (
            <div id="account-view" className="full-page">
                <div className="background account-box d-flex align-items-center justify-content-center">
                    <div className="box form-box">
                        {this.state.form}
                    </div>
                </div>
            </div>
        );
    }
}
import React from "react";
import {LoginForm} from "../views/forms/LoginForm";
import {PasswordMailForm} from "../views/forms/PasswordMailForm";
import {RegisterForm} from "../views/forms/RegisterForm";
import {MainRouter} from "../../MainRouter";
import background from "../../img/background.png"
import logo from "../../img/logo_white.png"
import {PasswordRecoverForm} from "../views/forms/PasswordRecoverForm";

export class LoginPage extends React.Component {

    constructor(props) {
        super(props);

        let form;
        switch (this.props.type) {
            default:
                form = (<LoginForm/>);
                break;
            case "forget":
                form = (<PasswordMailForm/>);
                break;
            case "recovery":
                form = (<PasswordRecoverForm/>);
                break;
            case "register":
                form = (<RegisterForm/>);
                break;
            case "logout":
                form = (<h1 className="text-center">Logging out...</h1>);
                MainRouter.logout();
                MainRouter.refresh("/login");
                break;
        }

        // Define state
        this.state = {form: form};
    }

    render() {
        return (
            <div id="login-view" className="full-page">
                <div className="background d-flex" style={{backgroundImage: `url('${background}')`}}>
                    <div className="logo-box part d-flex justify-content-center align-items-center px-100">
                        <img src={logo} alt="Hashlens" id={"logo"}/>
                    </div>
                    <div className="form part d-flex justify-content-center align-items-center">
                        <div className="form-box box p-20">
                            {this.state.form}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
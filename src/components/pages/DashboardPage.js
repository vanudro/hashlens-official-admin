import React from "react";
import $ from "jquery";
import {Header} from "../Header";
import {ChannelStatusBox} from "../elements/dashboard/ChannelStatusBox";
import {GlobalStatusBox} from "../elements/dashboard/GlobalStatusBox";
import {ButtonLink} from "../elements/form/ButtonLink";
import {App} from "../../js/App";

const api_list = {
    ID01: {
        name: "Facebook",
        icon: 'facebook',
        warnings: [],
        errors: []
    },
    ID02: {
        name: "Instagram",
        icon: 'instagram',
        warnings: [],
        errors: []
    },
    // ID03: {
    //     name: "LinkedIn",
    //     icon: 'linkedin',
    //     warnings: [],
    //     errors: []
    // },
    // ID04: {
    //     name: "Pinterest",
    //     icon: 'pinterest',
    //     warnings: [
    //         "Invalid properties"
    //     ],
    //     errors: []
    // },
    // ID05: {
    //     name: "Twitter",
    //     icon: 'twitter',
    //     warnings: [
    //         {message: "App key expired"}],
    //     errors: [
    //         "App version is out of date",
    //         "App key expired"]
    //
    // },
};
const api_list2 = [
    {
        name: "Facebook",
        type: 'facebook',
        status: {type: "error", message: "Invalid properties"}
    },
    {
        name: "Instagram",
        type: 'instagram',
        status: {type: "active", message: "All is good"}
    },
];

export class DashboardPage extends React.Component {

    constructor(props) {
        super(props);

        this.apis = {};


        this.state = this.getProcessedState(api_list2);
    }

    getProcessedState(list) {
        let api_count = list.length;
        let error_count = 0;
        let warning_count = 0;
        let active_count = 0;
        console.log(list);
        let count = 0;
        for (let key in list) {
            if (list.hasOwnProperty(key)) {
                let api = list[key];
                if (api.status.type === "error") {
                    error_count++;
                } else if (api.status.type === "warning") {
                    warning_count++;
                } else {
                    active_count++;
                }
                count++;
            }
        }

        let error_suffix = (error_count === 1)? "Error" : "Errors";
        let warning_suffix = (warning_count === 1)? "Warning" : "Warnings";
        let api_suffix = (api_count === 1)? "API" : "APIs";

        return {
            errors: error_count,
            warnings: warning_count,
            actives: active_count,
            total: count,
            error_suffix: error_suffix,
            warning_suffix: warning_suffix,
            api_suffix: api_suffix,
            list: list
        }
    }

    componentDidMount() {
        let that = this;
        $.ajax({
            url: App.getApiLink("channel/get/list"),
            method: "POST",
            contentType: "application/json",
            success: function (response) {
                console.log(response);
                if (response.status === "OK") {
                    let channels = [];
                    for (let i in response.data) {
                        if (response.data.hasOwnProperty(i)) {
                            let channel = response.data[i].channel;
                            console.log(channel);
                            channels.push(channel);
                        }
                    }
                    that.setState(that.getProcessedState(channels));
                }
            }
        })
    }

    render() {
        let list = (this.state.list != null)? this.state.list : [];
        let api_boxes = [];
        for (let key in list) {
            if (list.hasOwnProperty(key)) {
                let channel = list[key];
                console.log(channel);
                channel.icon = channel.type;
                channel.background = 'white';

                api_boxes.push(
                    <div key={key} className="p-10">
                        <ChannelStatusBox
                            title={channel.name}
                            type={channel.type}
                            icon={`fab fa-${channel.icon}`}
                            status={channel.status}
                            background={channel.background}
                            message={channel.status.message}/>
                    </div>
                )
            }
        }

        return (
            <div id="dashboard-view" className="full-page">
                <div className="wrapper container">
                    <div className="px-10">
                        <h1>Dashboard</h1>
                    </div>
                    <div className="top d-flex">
                        <div className="d-flex p-10">
                            <div className="box p-20">
                                <Header title={"Goodmoring"} sub_title={"Robin"}/>
                                <p>You have currently <span className="text-primary">{this.state.total}</span> {this.state.api_suffix} working efficiently.</p>
                            </div>
                        </div>
                        <div className="status-box d-flex p-10">
                            <GlobalStatusBox
                                class={(this.state.actives > 0)? "text-success" : "text-default"}
                                icon={`far fa-${(this.state.actives > 0)? "laugh-beam" : "sad-cry"}`}
                                title={"Active"}
                                value={this.state.actives}
                            />
                        </div>
                        <div className="status-box d-flex p-10">
                            <GlobalStatusBox
                                class={(this.state.warnings > 0)? "text-warning" : "text-default"}
                                icon={`far fa-bell`}
                                title={"Warnings"}
                                value={this.state.warnings}
                            />
                        </div>
                        <div className="status-box d-flex p-10">
                            <GlobalStatusBox
                                class={(this.state.errors > 0)? "text-error" : "text-default"}
                                icon={`far fa-bomb`}
                                title={"Errors"}
                                value={this.state.errors}
                            />
                        </div>
                    </div>
                    <div className="pt-30">
                        <div className="px-10">
                            <h3>Installed API's</h3>
                        </div>
                        <div className="boxes d-flex d-flex flex-wrap">
                            {api_boxes}
                            <div className="p-10">
                                <div id="add-api" className="api-box box h-100 d-flex justify-content-center align-items-center">
                                    <ButtonLink icon={"far fa-plus"} link={"/api/add"} class={"primary link text-super"}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
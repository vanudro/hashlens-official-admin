import React from "react";
import {ChannelEditForm} from "../views/forms/ChannelEditForm";
import {ChannelListView} from "../views/ChannelListView";
import $ from "jquery";
import {App} from "../../js/App";
import {ChannelTokenForm} from "../views/forms/ChannelTokenForm";
import {ChannelDeleteForm} from "../views/forms/ChannelDeleteForm";

export class ChannelPage extends React.Component {

    constructor(props) {
        super(props);
        this.toggleOnline = this.toggleOnline.bind(this);
        let actions = {
            onToggle: this.toggleOnline,
            deleteChannel: this.deleteChannel,
        };

        let view, type = (this.props.type != null)? this.props.type : null;
        switch (type) {
            case 'management':
                view = (<ChannelListView actions={actions}/>);
                break;
            case 'authorize':
                view = (<ChannelTokenForm/>);
                break;
            case 'delete':
                view = (<ChannelDeleteForm actions={actions}/>);
                break;
            default:
                view = (<ChannelEditForm actions={actions}/>);
                break;
        }
        this.state = {view: view};
    }

    // TODO: Implement toggle in list
    toggleOnline(id, success) {
        $.ajax({
            url: App.getApiLink("channel/toggle/" + id),
            method: "POST",
            contentType: "application/json",
            success: function (response) {
                console.log(response);
                if (success != null && response.status === "OK") {
                    let channel = response.data[0].channel;
                    success(channel);
                }
            },
            error: function (response) {
                console.log(response)
            }
        })
    }

    // TODO: Implement toggle in list
    deleteChannel(id, success) {
        $.ajax({
            url: App.getApiLink("channel/delete/" + id),
            method: "POST",
            contentType: "application/json",
            success: function (response) {
                console.log(response);
                if (success != null && response.status === "OK") {
                    let channel = response.data[0].channel;
                    success(channel);
                }
            },
            error: function (response) {
                console.log(response)
            }
        })
    }

    render() {
        return (
            <div id="api-view" className="full-page">
                <div className="background d-flex align-items-center justify-content-center">
                    {this.state.view}
                </div>
            </div>
        );
    }
}
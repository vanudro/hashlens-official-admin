import React from "react";
import {Header} from "../Header";
import {App} from "../../js/App";
import $ from 'jquery';
import logo from "../../img/logo_black.png";
import {ButtonLink} from "../elements/form/ButtonLink";

export class ActivationPage extends React.Component {

    constructor(props) {
        super(props);
        let url = new URL(window.location.href);
        let hash = url.searchParams.get("token");
        console.log(hash);
        this.state = {title: "Activate account", sub: "", type: "text-error", hash: hash}
    }

    componentDidMount() {
        let that = this;
        $.ajax({
            url: App.getApiLink(`account/validate/${that.state.hash}/`),
            method: "GET",
            contentType: "application/json",
            success: function (response) {
                let message = response.messages[0];
                if (response.status === "OK") {
                    that.setState({title: "Welcome to the club!", sub: message.message, type: `text-${message.type}`, hash: that.state.hash});
                } else {
                    that.setState({title: "Something went wrong", sub: message.message, type: `text-${message.type}`, hash: that.state.hash});
                }
            },
            error: function (response) {
                that.setState({title: "Something went wrong", sub: `An error has been found (${this.hash})`});
            }
        })
    }


    render() {
        return (
            <div id="activation-view" className="d-flex justify-content-center align-items-center full-page background-grey">
                <div className="box small rounded shadow p-20">
                    <img src={logo} alt=""/>
                    <div className="py-20">
                        <Header title={this.state.title} sub_title={this.state.sub} sub_class={this.state.type}/>
                    </div>
                    <ButtonLink text={"Go back to login"} link="/login" class="primary link"/>
                </div>
            </div>
        );

    }

}
import React from "react";
import {Header} from "../../Header";

export class GlobalStatusBox extends React.Component {

    constructor(props) {
        super(props);
        this.state = this.getStateFromProps(this.props);
    }

    getStateFromProps(props) {
        let icon = (props.icon != null)? props.icon : 'far fa-circle';
        let title = (props.title != null)? props.title : 'title';
        let _class = (props.class != null)? props.class : 'text-default';
        let value = (props.value != null)? props.value : 0;
        return this.getState(icon, title, _class, value);
    }

    getState(icon, title, _class, value) {
        return {icon: icon, title: title, class: _class, value: value};
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.icon !== this.state.icon) {
            return true;
        }
        if (nextProps.title !== this.state.title) {
            return true;
        }
        if (nextProps.value !== this.state.value) {
            return true;
        }
        return false;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.setState(this.getStateFromProps(this.props));
    }

    render() {
        return (
            <div className="box small status d-flex flex-direction-column justify-content-flex-end p-20">
                <i className={`icon ${this.state.icon} ${this.state.class} pb-20`}/>
                <Header title={this.state.value} sub_title={this.state.title} flip={true}/>
            </div>
        );
    }

}
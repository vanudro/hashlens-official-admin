import React from "react";
import {Header} from "../../Header";

export class ChannelStatusBox extends React.Component {
    constructor(props) {
        super(props);

        this.state = this.getStateFromProps(this.props);
    }

    getStateFromProps(props) {
        let icon = (props.icon != null)? props.icon : 'far fa-circle';
        let title = (props.title != null)? props.title : 'title';
        let status = (props.status != null)? props.status : 'Not configured';
        let color = (props.color != null)? props.color : 'white';
        let text = (props.text != null)? props.text : 'black';
        let type = (props.type != null)? props.type : 'error';
        let message = (props.message != null)? props.message : '';

        return this.getState(icon, title, status, type, text, color, message);
    }

    getState(icon, title, status, type, text, color, message) {
        return {icon: icon, title: title, status: status, type: type, text: text, color: color, message: message}
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.setState(this.getStateFromProps(this.props));
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.title !== this.props.title) {
            return true;
        }
        if (nextProps.title !== this.state.title) {
            return true;
        }
        if (nextProps.type !== this.props.type) {
            return true;
        }
        if (nextProps.type !== this.state.type) {
            return true;
        }
        return false;
    }

    render() {
        let type = this.state.status.type;
        if (this.state.status.type === "active") {
            type = "success";
        }
        return (
            <div className="api-box box d-flex flex-direction-column justify-content-space-between p-20">
                <div className="box-content d-flex">
                    <Header title={this.state.title} sub_title={this.state.status.name} sub_class={`text-${type}`}/>
                    <i className={`icon ${this.state.icon} text-${this.state.type}`}/>
                </div>
                <div className="pt-10">
                    <p className={`h5 text-grey`}>{this.state.message}</p>
                </div>
            </div>
        );
    }
}
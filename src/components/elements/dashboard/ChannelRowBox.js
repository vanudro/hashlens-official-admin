import React from "react";
import {IconButton} from "../form/IconButton";

export class ChannelRowBox extends React.Component {

    constructor(props) {
        super(props);
        this.toggleOnline = this.toggleOnline.bind(this);
        this.onToggle = this.onToggle.bind(this);

        let channel = (this.props.channel != null)? this.props.channel : {status: {}};
        this.state = this.getState(channel);
    }

    getState(channel) {
        let _class;
        switch (channel.status.type) {
            case "active":
                _class = 'success';
                break;
            case "offline":
                _class = 'grey';
                break;
            default:
                _class = channel.status.type;
                break;
        }
        return {channel: channel, class: _class};
    }

    toggleOnline() {
        this.props.onToggle(this.state.channel.id, this.onToggle);
    }

    onToggle(channel) {
        this.setState(this.getState(channel))
    }

    render() {
        let channel = (this.state.channel != null)? this.state.channel : {};
        console.log(channel);
        return (
            <div className="box api-row mb-10 d-flex justify-content-space-between">
                <div className="cell d-flex">
                    <div className="icon">
                        <i className={`icon fab fa-${channel.type} text-${channel.type}`}/>
                    </div>
                    <div className="data title">
                        <h3>{channel.name}</h3>
                    </div>
                    <div className="data status">
                        <h4 className={`text-${this.state.class}`}>
                            {(channel.status.name === "Error" || channel.status.name === "Warning")? channel.status.message : channel.status.name}
                        </h4>
                    </div>
                </div>
                <div className="cell actions buttons pr-10">
                    <IconButton
                        icon={`far fa-${channel.active? "toggle-on" : "toggle-off"}`}
                        class={`${channel.active? "success" : "error"} link`}
                        onClick={this.toggleOnline}/>
                    <IconButton
                        icon={`far fa-${(channel.token === "")? "shield" : "shield-check"}`}
                        class={`link ${(channel.token === "")? "" : "success"}`}
                        link={`/api/authorize/${channel.type}`}/>
                    <IconButton icon={`far fa-pencil`} class="info link" link={`/api/edit/${channel.type}`}/>
                    <IconButton icon={`far fa-trash-alt`} class="error link" link={`/api/delete/${channel.type}`}/>
                </div>
            </div>
        );
    }

}
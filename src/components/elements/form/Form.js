import React from "react";

export class Form extends React.Component {

    constructor(props) {
        super(props);
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {messages: {}, default_messages: {}, button: {type: "", text: "", icon: ""}};
    }

    handleInput(e) {
        let name = e.target.name;
        let value = e.target.value;
        let state = this.state;
        state[name] = value;
        this.setState(state);
    }

    setMessage(name, type, message, _default = false) {
        let msg = {type: type, message: message};
        let state = this.state;
        state.messages[name] = msg;
        if (_default) {
            state.default_messages[name] = msg;
        }
        this.setState(state);
    }

    getMessageText(name) {
        if (this.state.messages.hasOwnProperty(name)) {
            return this.state.messages[name].message;
        }
        return null;
    }

    getMessageType(name) {
        if (this.state.messages.hasOwnProperty(name)) {
            return this.state.messages[name].type;
        }
        return null;
    }

    resetMessages() {
        let state = this.state;
        state.messages = null;
        state.messages = JSON.parse(JSON.stringify(state.default_messages));
        this.setState(state);
    }

    setButton(icon, text, type, _default = false) {
        let button = {icon: `far fa-${icon}`, text: text, type: type};
        let state = this.state;
        state.button = button;
        if (_default) {
            state.default_button = button;
        }
        this.setState(state);
    }

    getButtonIcon() {
        return this.state.button.icon;
    }

    getButtonType() {
        return this.state.button.type;
    }

    getButtonText() {
        return this.state.button.text;
    }

    setProcessButton(text = "Saving") {
        let that = this;
        this.setButton("spin fa-sync", text, "primary");
        setTimeout(function () {
            that.resetButton();
        }, 2000);
    }

    resetButton() {
        let state = this.state;
        state.button = null;
        state.button = JSON.parse(JSON.stringify(state.default_button));
        this.setState(state);
    }

    handleSubmit(e) {
        e.preventDefault();
        this.onSubmit(this);
    }

    onSubmit() {}
}
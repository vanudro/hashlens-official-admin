import React from "react";

export class Button extends React.Component {

    constructor(props) {
        super(props);

        // Binds
        this.handleClick = this.handleClick.bind(this);

        // Define state
        this.state = this.getStateFromProps(this.props);
    }

    getState(text, _class, icon, onClick) {
        return {text: text, class: _class, icon: icon, onClick: onClick};
    }

    getStateFromProps(props) {
        let text = (props.text != null)? props.text : null;
        let _class = (props.class != null)? props.class : 'primary';
        let icon = (props.icon != null)? props.icon : false;
        let onClick = (props.onClick != null)? props.onClick : null;

        return this.getState(text, _class, icon, onClick);
    }

    setButton(text, _class, icon, onClick) {
        this.setState(this.getState(text, _class, icon, onClick));
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.setState(this.getStateFromProps(this.props));
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.onClick != null && this.props.onClick == null) {
            return true;
        }
        if (nextProps.text != null && nextProps.text !== this.props.text) {
            console.log("next text");
            return true;
        }
        if (nextProps.icon != null && nextProps.icon !== this.props.icon) {
            console.log("next icon");
            return true;
        }
        if (nextProps.class != null && nextProps.class !== this.props.class) {
            console.log("next class");
            return true;
        }
        if (this.props.text != null && this.props.text !== this.state.text) {
            console.log("current text");
            return true;
        }
        if (this.props.icon != null && this.props.icon !== this.state.icon) {
            console.log("current icon");
            return true;
        }
        if (this.props.class != null && this.props.class !== this.state.class) {
            console.log("current class");
            return true;
        }
        return false;
    }

    handleClick() {
        if (this.state.onClick != null) {
            this.state.onClick();
        } else {
            console.log("onClick not found...");
        }
    }

    renderOld() {
        if (this.state.icon && this.state.text == null && this.state.link != null) {
            // Link icon only
            return (
                <a className={`button icon-only ${this.state.class}`} href={this.state.link}>
                    <i className={this.state.icon}/>
                </a>
            );
        } else if (this.state.icon && this.state.text != null && this.state.link != null) {
            // Link icon and text
            return (
                <a className={`button ${this.state.class}`} href={this.state.link}>
                    <i className={`icon ${this.state.icon}`}/>
                    <span>{this.state.text}</span>
                </a>
            );
        } else if (this.state.icon && this.state.text == null) {
            // Button icon only
            return (
                <button className={`button icon-only ${this.state.class}`} onClick={this.handleClick}>
                    <i className={this.state.icon}/>
                </button>
            );
        } else if (this.state.icon) {
            // Button icon and text
            return (
                <button className={`button ${this.state.class}`} onClick={this.handleClick}>
                    <i className={`icon ${this.state.icon}`}/>
                    <span>{this.state.text}</span>
                </button>
            );
        } else if (!this.state.icon && this.state.link != null && this.state.text != null) {
            // Link text only
            return (
                <a className={`button ${this.state.class}`} href={this.state.link}>
                    <span>{this.state.text}</span>
                </a>
            );
        } else {
            // Button text only
            return (<button className={`button ${this.state.class}`} onClick={this.handleClick}>{this.state.text}</button>);
        }
    }

    render() {
        if (this.state.icon) {
            return (
                <button className={`button ${this.state.class}`} onClick={this.handleClick}>
                    <i className={`icon ${this.state.icon}`}/>
                    <span>{this.state.text}</span>
                </button>
            );
        } else {
            return (
                <button className={`button ${this.state.class}`} onClick={this.handleClick}>{this.state.text}</button>
            );
        }
    }
}
import React from "react";

export class Input extends React.Component {

    constructor(props) {
        super(props);

        // Binds
        this.handleOnChange = this.handleOnChange.bind(this);
        this.setValue = this.setValue.bind(this);

        // Define state
        this.state = this.getStateFromProps(this.props);
    }

    getState(label, value, message, name, type, _class, disabled, onChange) {
        return {label: label, value: value, message: message, name: name, class: _class, type: type, disabled: disabled, onChange: onChange};
    }

    getStateFromProps(props) {
        // Get props
        let label = (props.label != null)? props.label : 'title';
        let value = (props.value != null)? props.value : '';
        let message = (props.message != null)? props.message : '';
        let name = (props.name != null)? props.name : '';
        let type = (props.type != null)? props.type : 'text';
        let _class = (props.class != null)? props.class : 'default';
        let disabled = (props.disabled != null)? props.disabled : false;
        let onChange = (props.onChange != null)? props.onChange : null;

        return this.getState(label, value, message, name, type, _class, disabled, onChange);
    }

    setValue(value) {
        let state = this.state;
        state.value = value;
        this.setState(state);
    }

    setStatus(_class, message) {
        let state = this.state;
        state.message = message;
        state.class = _class;
        this.setState(state);
    }

    handleOnChange(e) {
        if (this.state.onChange != null) {
            this.state.onChange(e);
            this.setValue(e.target.value);
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState(this.getStateFromProps(nextProps));
    }

    render() {
        return (
            <div className={`input-row pt-20`}>
                <input
                    id={`input-${this.state.name}`}
                    className={`input ${this.state.class}`}
                    type={this.state.type}
                    name={this.state.name}
                    placeholder={this.state.label}
                    value={this.state.value}
                    disabled={this.state.disabled}
                    onChange={this.handleOnChange}/>
                <label htmlFor={`input-${this.state.name}`} className="helper">{this.state.message}</label>
            </div>
        );
    }
}
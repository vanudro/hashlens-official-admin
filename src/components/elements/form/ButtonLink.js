import React from "react";
import {Button} from "./Button";

export class ButtonLink extends Button {

    constructor(props) {
        super(props);

        // Binds
        this.handleClick = this.handleClick.bind(this);

        // Define state
        this.state = this.getStateFromProps(this.props);
    }

    getStateFromProps(props) {
        let state = super.getStateFromProps(props);
        state.link = (props.link != null)? props.link : null;

        return state;
    }

    setButton(text, _class, icon, link) {
        let state = this.getState(text, _class, icon);
        state.link = link;
        this.setState(state);
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (super.shouldComponentUpdate(nextProps, nextState, nextContext)) {
            return true;
        }
        if (this.props.link != null && this.props.link !== this.state.link) {
            console.log("current link");
            return true;
        }
        if (nextProps.link != null && nextProps.link !== this.state.link) {
            console.log("next link");
            return true;
        }
        return false;
    }

    componentDidUpdate() {
        this.setState(this.getStateFromProps(this.props));
    }

    render() {
        if (this.state.icon) {
            return (
                <a className={`button ${this.state.class}`} href={this.state.link}>
                    <i className={`icon ${this.state.icon}`}/>
                    <span>{this.state.text}</span>
                </a>
            );
        } else {
            return (
                <a className={`button ${this.state.class}`} href={this.state.link}>{this.state.text}</a>
            );
        }
    }
}
import React from "react";
import {Input} from "./Input";

export class Select extends Input {

    constructor(props) {
        super(props);
        this.state = this.getStateFromProps(this.props);
    }

    getStateFromProps(props) {
        let state = super.getStateFromProps(props);
        state.options = props.options;
        return state;
    }

    handleOnChange(e) {
        if (this.state.onChange != null) {
            this.state.onChange(e);
            this.setValue(e.target.value);
        } else {
            console.log("onChange not found...");
            console.log(this.state);
        }
    }

    render() {
        let options = [];
        options.push(<option key={""} value={""}>{`Select a ${this.state.label}`}</option>)
        let options_raw = this.state.options;
        for (let i in options_raw) {
            if (options_raw.hasOwnProperty(i)) {
                let option = options_raw[i];
                options.push(<option key={i} value={option.key}>{option.value}</option>)
            }
        }

        return (
            <div className={`input-row pt-20`}>
                <select
                    id={`input-${this.state.name}`}
                    className={`input ${this.state.class}`}
                    value={this.state.value}
                    name={this.state.name}
                    placeholder={this.state.label}
                    onChange={this.handleOnChange}>
                    {options}
                </select>
                <label htmlFor={`input-${this.state.name}`} className="helper">{this.state.message}</label>
            </div>
        );
    }
}
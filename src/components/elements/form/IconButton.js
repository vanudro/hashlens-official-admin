import React from "react";
import {ButtonLink} from "./ButtonLink";

export class IconButton extends ButtonLink {

    render() {
        if (this.state.link != null) {
            return (
                <a className={`button icon-only ${this.state.class}`} href={this.state.link}>
                    <i className={`${this.state.icon}`}/>
                </a>
            );
        } else {
            return (
                <button className={`button icon-only ${this.state.class}`} onClick={this.handleClick}>
                    <i className={`${this.state.icon}`}/>
                </button>
            );
        }
    }
}
import React from "react";

export class Header extends React.Component {

    constructor(props) {
        super(props);

        this.state = this.getStateFromProps(this.props);
    }

    getStateFromProps(props) {
        let title = (props.title != null)? props.title : null;
        let _class = (props.class != null)? props.class : null;
        let sub_title = (props.sub_title != null)? props.sub_title : null;
        let sub_class = (props.sub_class != null)? props.sub_class : null;
        let flip = (props.flip != null)? props.flip : null;
        return {title: title, class: _class, sub_title: sub_title, sub_class: sub_class, flip: flip};
    }

    setHeader(title, sub_title) {
        this.setState({title: title, sub_title: sub_title});
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.setState(this.getStateFromProps(this.props));
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (nextProps.title != null && nextProps.title !== this.state.title) {
            return true;
        }
        if (nextProps.sub_title != null && nextProps.sub_title !== this.state.sub_title) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <div className="header ">
                <div className={`titles d-flex flex-direction-column${this.state.flip? "-reverse" : ""}`}>
                    <div>
                        <h2 className={`title ${this.state.class}`}>{this.state.title}</h2>
                    </div>
                    <h4 className={`alt ${this.state.sub_class}`}>{this.state.sub_title}</h4>
                </div>
            </div>
        );
    }
}
import React from "react";

export class Avatar extends React.Component {

    constructor(props) {
        super(props);
        this.state = this.getState(this.props);
    }

    getState(props) {
        let img = "https://images.pexels.com/photos/2091327/pexels-photo-2091327.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260";
        let image = (props.image != null)? props.image : img;
        let alt = (props.alt != null)? props.alt : '';
        return {image: image, alt: alt};
    }

    componentDidUpdate() {
        this.setState(this.getState(this.props));
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.image != null && nextProps.image !== this.state.image) {
            return true;
        }
        if (nextProps.alt != null && nextProps.alt !== this.state.alt) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <div className="avatar d-flex justify-content-center">
                <img src={this.state.image} alt={this.state.alt} className={"image shadow rounded"}/>
            </div>
        );
    }
}
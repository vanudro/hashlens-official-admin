import React from "react";
import logo from "../img/logo_black.png";

export class LogoImage extends React.Component {
    render() {
        return (
            <img src={logo} alt="Hashlens logo" className="logo"/>
        );
    }
}
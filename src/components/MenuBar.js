import React from "react";
import {Link} from "react-router-dom";
import {LogoImage} from "./LogoImage";

const navigation = [
    {
        title: "Dashboard",
        icon: "tachometer",
        url: "/",
        position: "top"
    },
    {
        title: "Api Management",
        icon: "plug",
        url: "/api/management",
        position: "top"
    },
    {
        title: "Account",
        icon: "user",
        url: "/account",
        position: "bottom"
    },
    {
        title: "Log out",
        icon: "sign-out",
        url: "/logout",
        position: "bottom"
    }
];

export class MenuBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {items: navigation};
    }

    getItems(position) {
        let items = [];
        for (let i in this.state.items) {
            let item = this.state.items[i];
            if (item.position === position) {
                items.push(
                    <li key={i} className="mx-20">
                        <Link to={item.url}>
                            <i className={`icon far fa-${item.icon}`}/><span>{item.title}</span>
                        </Link>
                    </li>
                );
            }
        }
        return items;
    }


    render() {
        return (
            <div className="menu-bar">
                <div className="bar">
                    <div className="top">
                        <div className="logo p-20">
                            <LogoImage/>
                        </div>
                        <div className="navigation">
                            <ul>
                                {this.getItems("top")}
                            </ul>
                        </div>
                    </div>
                    <div className="bottom">
                        <div className="navigation pb-20">
                            <ul>
                                {this.getItems("bottom")}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
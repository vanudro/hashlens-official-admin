import React from "react";
import $ from "jquery";
import {Button} from "../../elements/form/Button";
import {Header} from "../../Header";
import {App} from "../../../js/App";
import {MainRouter} from "../../../MainRouter";
import {IconButton} from "../../elements/form/IconButton";
import {ButtonLink} from "../../elements/form/ButtonLink";
import {Form} from "../../elements/form/Form";

export class ChannelDeleteForm extends Form {

    getState(channel, is_deleted) {
        return {channel: channel, is_deleted: is_deleted};
    }

    componentDidMount() {
        let params = window.location.pathname.split("/");
        let name = params[params.length - 1];

        let that = this;
        $.ajax({
            url: App.getApiLink(`channel/find/name/${name}`),
            method: "DELETE",
            contentType: "application/json",
            success: function (response) {
                console.log(response);
                if (response.status === "OK") {
                    let channel = response.data[0].channel;
                    that.setState(that.getState(channel, false));
                } else {
                    MainRouter.refresh("/api/management", 2000);
                    that.setState(that.getState({type: ""}, true));
                }
            }
        });
    }

    onSubmit() {
        this.deleteChannel();

    }

    deleteChannel() {
        this.props.actions.deleteChannel(this.state.channel.id, this.onDeleteUser);
    }

    onDeleteUser() {
        MainRouter.refresh("/api/management")
    }

    render() {
        if (this.state.channel != null) {
            console.log(this.state.channel);
            let link_back = (this.state.is_deleted)? `/api/management` : `/api/edit/${this.state.channel.type}`;
            return (
                <div className="box form-box">
                    <form action="#" method="post" onSubmit={this.handleSubmit}>
                        <div className="buttons top-bar d-flex justify-content-space-between p-10">
                            <IconButton icon="far fa-arrow-left" class="link" link={link_back}/>
                        </div>
                        <div className="p-20">
                            <Header
                                title={(this.state.is_deleted)? `Channel deleted` : `Delete ${this.state.channel.name}`}
                                sub_title={(this.state.is_deleted)? "Going back now" : "This will permanently delete the channel"}/>
                            <div className="buttons big pt-40 d-flex justify-content-space-between">
                                <Button text={"Delete"} icon={"far fa-trash-alt"} class="error"/>
                                <ButtonLink text={"Cancel"} link={link_back} class="default"/>
                            </div>
                        </div>
                    </form>
                </div>
            );
        } else return null;
    }

}
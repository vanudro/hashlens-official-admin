import React from "react";
import {Button} from "../../elements/form/Button";
import {Header} from "../../Header";
import {Input} from "../../elements/form/Input";
import {App} from "../../../js/App";
import $ from 'jquery';
import {Form} from "../../elements/form/Form";
import {MainRouter} from "../../../MainRouter";
import {IconButton} from "../../elements/form/IconButton";

const EMPTY_CHANNEL = {
    type: "",
    status: {},
    access_token: ""
};

export class ChannelTokenForm extends Form {

    componentDidMount() {
        this.setMessage("token", "warning", "No access token found", true);
        this.setButton("paper-plane", "Reset token", "primary", true);

        this.getData();
    }

    getStateProps(props) {
        let id = (props.id != null)? props.id : "";
        let type = (props.type != null)? props.type : "";
        let token = (props.token != null && props.token !== "")? props.token : "No access token";

        return this.getState(id, type, token);
    }

    getState(id, type, token) {
        return {id: id, type: type, token: token};
    }

    onSubmit() {
        this.checkURL(true);
    }

    checkURL(send = false) {
        let url = window.location.href;
        if (url.indexOf("#") > -1) {
            window.location.href = url.replace("#", '');
        } else {
            let token, params = new URL(url).searchParams;
            if (params.get("code") != null) {
                token = params.get("code");
            } else if (params.get("access_token") != null) {
                token = params.get("access_token");
            }
            console.log(window.location.hash);
            if (token != null) {
                this.setAccessToken(token);
            } else if (send) {
                this.sendData();
            }
        }
    }

    setAccessToken(token) {
        let that = this;
        let json = {token: token};
        $.ajax({
            url: App.getApiLink("channel/put/token/" + that.state.channel.id),
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (response) {
                if (response.status === "OK") {
                    that.resetMessages();
                    that.setButton("check", "Token saved", "success");
                    MainRouter.refresh(`/api/authorize/${that.state.channel.type}`, 2000);
                } else {
                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            that.handleStatus("token", message.type, message.message);

                        }
                    }
                    that.setButton("times", "Check the form", "error");
                }
            }
        });
    }

    getData() {
        let that = this;

        let params = window.location.pathname.split("/");
        let name = params[params.length - 1];
        $.ajax({
            url: App.getApiLink("channel/find/name/" + name),
            method: "POST",
            contentType: "application/json",
            success: function (response) {
                if (response.status === "OK") {
                    let channel = response.data[0].channel;
                    console.log(channel);
                    let state = that.getStateProps(channel);
                    state.channel = channel;
                    that.setState(state);
                    that.checkURL();
                    if (channel.access_token == null || channel.access_token === "") {
                        that.setButton("paper-plane", "Request new token", "primary", true);
                    } else {
                        that.setMessage("token", "warning", "No access token found", true);
                    }
                } else {
                    let state = that.state;
                    state.is_new = true;
                    that.setState(state);
                }
            }
        });
    }

    sendData() {
        let that = this;
        let json = {link: encodeURIComponent(App.getLink(`api/authorize/${that.state.channel.type}`))};
        $.ajax({
            url: App.getApiLink("channel/get/auth_url/" + that.state.channel.id),
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (response) {
                console.log(response);
                if (response.status === "OK") {
                    let url = response.data[0].replace(/['"]+/g, '');
                    window.location.href = url;
                } else {
                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            that.setMessage("token", message.type, message.message);

                        }
                    }
                    that.setButton("times", "Check the form", "error");
                }
            },
            error: function (response) {
                console.log(response);
                that.setButton("times", "Something went wrong...", "error");
            }
        });
    }

    render() {
        console.log(this.state);
        let channel = (this.state.channel != null)? this.state.channel : EMPTY_CHANNEL;
        return (
            <div className="box form-box">
                <form action="#" method="post" onSubmit={this.handleSubmit}>
                    <div className="box">
                        <div className="p-10 buttons d-flex justify-content-space-between">
                            <IconButton icon="far fa-arrow-left" link={`/api/edit/${channel.type}`} class="link"/>
                        </div>
                        <div className="p-20">
                            <Header
                                title={(channel.token === "")? `New access token` : `Current access token`}
                                sub_title={channel.status.message}
                                sub_class={`text-${channel.status.type}`}/>
                            <Input
                                label={"Access token"}
                                name={'token'}
                                value={this.state.token}
                                message={this.getMessageText("token")}
                                class={this.getMessageType("token")}
                                disabled={true}
                                onChange={this.handleInput}/>
                            <div className="buttons pt-40 d-flex justify-content-space-between">
                                <div className="form">
                                    <Button
                                        icon={this.getButtonIcon()}
                                        text={this.getButtonText()}
                                        class={this.getButtonType()}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
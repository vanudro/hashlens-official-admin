import React from "react";
import $ from "jquery";
import {Button} from "../../elements/form/Button";
import {Header} from "../../Header";
import {Input} from "../../elements/form/Input";
import {App} from "../../../js/App";
import {MainRouter} from "../../../MainRouter";
import {IconButton} from "../../elements/form/IconButton";
import {Form} from "../../elements/form/Form";

export class PasswordChangeForm extends Form {

    getState(old_password, password, password_retype) {
        return {old_password: old_password, password: password, password_retype: password_retype}
    }

    componentDidMount() {
        this.setMessage("old_password", "default", "Enter your current password.", true);
        this.setMessage("password", "default", "Enter a new password.", true);
        this.setMessage("password_retype", "default", "Retype your new password.", true);
        this.setButton("save", "Save", "primary", true);
    }

    onSubmit(that) {
        let old_password = (this.state.old_password != null)? this.state.old_password : null;
        let password = (this.state.password != null)? this.state.password : null;
        let password_retype = (this.state.password_retype != null)? this.state.password_retype : null;

        this.resetMessages();
        setTimeout(function () {
            if (that.checkForm(old_password, password, password_retype)) {
                that.updatePassword(old_password, password);
            } else {
                that.setButton("far fa-times", "Failed...", "error");
            }
        }, 500);
        this.setProcessButton();
    }

    checkForm(old_password, password, password_retype) {
        if (old_password == null || old_password === "") {
            this.setMessage("old_password", "error", "Please enter your current password.");
            return false;
        }
        if (password == null || password === "") {
            this.setMessage("password", "error", "Please enter a new password.");
            return false;
        }
        if (password_retype == null || password_retype === "") {
            this.setMessage("password_retype", "error", "Retype your new password.");
            return false;
        }
        if (password !== password_retype) {
            this.setMessage("password", "error", "Passwords don't match.");
            this.setMessage("password_retype", "error", "Passwords don't match.");
            return false;
        }
        return true;
    }

    updatePassword(old_password, password) {
        let that = this;
        $.ajax({
            url: App.getApiLink("user/password/change/" + MainRouter.getID()),
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify({
                old_password: old_password,
                password: password
            }),
            success: function (response) {
                console.log("Whohoo");
                console.log(response);

                if (response.status === "OK") {
                    that.setButton("far fa-check", "Changed password!", "success");
                } else {
                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            console.log(message);
                            if (message.field === "old_password") {
                                that.setMessage("old_password", message.type, message.message);
                            } else {
                                that.setMessage("password", message.type, message.message);
                            }
                        }
                    }
                    that.setButton("far fa-times", "Check the form", "error");
                }
            },
            error: function (response) {
                that.setButton("far fa-times", "Saving failed...", "error");
            }
        });
    }

    render() {
        return (
            <form action="#" method="post" onSubmit={this.handleSubmit}>
                <div className="box">
                    <div className="p-10 buttons">
                        <IconButton icon="far fa-arrow-left" link={"/account"} class="link"/>
                    </div>
                    <div className="p-20">
                        <Header
                            title={`Change password`} sub_title={"Think of something you can remember"}/>
                        <Input
                            label={"Current password"}
                            type={"password"}
                            name={'old_password'}
                            value={this.state.old_password}
                            message={this.getMessageText("old_password")}
                            class={this.getMessageType("old_password")}
                            onChange={this.handleInput}/>
                        <Input
                            label={"New password"}
                            type={"password"}
                            name={'password'}
                            value={this.state.password}
                            message={this.getMessageText("password")}
                            class={this.getMessageType("password")}
                            onChange={this.handleInput}/>
                        <Input
                            label={"Retype"}
                            type={"password"}
                            name={'password_retype'}
                            value={this.state.password_retype}
                            message={this.getMessageText("password_retype")}
                            class={this.getMessageType("password_retype")}
                            onChange={this.handleInput}/>
                        <div className="buttons pt-40 d-flex justify-content-space-between">
                            <div className="form">
                                <Button
                                    icon={this.getButtonIcon()}
                                    text={this.getButtonText()}
                                    class={this.getButtonType()} />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }

}
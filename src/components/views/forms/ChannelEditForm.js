import React from "react";
import {Button} from "../../elements/form/Button";
import {Header} from "../../Header";
import {Input} from "../../elements/form/Input";
import {App} from "../../../js/App";
import $ from 'jquery';
import {Select} from "../../elements/form/Select";
import {Form} from "../../elements/form/Form";
import {MainRouter} from "../../../MainRouter";
import {IconButton} from "../../elements/form/IconButton";

export class ChannelEditForm extends Form {

    constructor(props) {
        super(props);
        this.handleForm = this.handleForm.bind(this);
        this.toggleOnline = this.toggleOnline.bind(this);
        this.onToggle = this.onToggle.bind(this);
        this.sendData = this.sendData.bind(this);

        let params = window.location.pathname.split("/");
        let name = params[params.length - 1];
        console.log(name);

        let state = this.getStateProps({});
        state.slug = name;
        state.is_new = (name == null);
        state.status = this.resetMessages();
        state.button = this.getNormalButton();
        state.channel = {type: null, name: null, status: {type: null, message: null}};
        this.state = state;
    }

    setMessages(type, app, secret, data_url) {
        return {type: {message: type}, app_id: {message: app}, secret: {message: secret}, data_url: {message: data_url}};
    }

    resetMessages() {
        return this.setMessages(
            "Select a channel type",
            "Enter a app/ client id.",
            "Enter a secret id.",
            "Enter an url to retrieve data from."
        );
    }

    getNormalButton() {
        return this.getButton("far fa-save", "primary", "Save");
    }

    getButton(icon, type, text) {
        return {icon: icon, type: type, text: text};
    }

    setButton(icon, type, text) {
        let state = this.state;
        state.button = this.getButton(icon, type, text);
        this.setState(state);
    }

    resetButton() {
        let state = this.state;
        state.button = this.getNormalButton();
        this.setState(state);
    }

    getStateProps(props) {
        let id = (props.id != null)? props.id : "";
        let app = (props.app_id != null)? props.app_id : "";
        let secret = (props.secret != null)? props.secret : "";
        let type = (props.type != null)? props.type : "";
        let data_url = (props.data_url != null)? props.data_url : "";
        let active = (props.active != null)? props.active : false;

        return this.getState(id, app, secret, type, data_url, active);
    }

    getState(id, app, secret, type, data_url, active) {
        return {id: id, app_id: app, secret: secret, type: type, data_url: data_url, active: active};
    }

    componentDidMount() {
        this.getTypes();
        this.getData();
    }

    getTypes() {
        let that = this;
        $.ajax({
            url: App.getApiLink("channel/get/types"),
            method: "GET",
            contentType: "application/json",
            success: function (response) {
                if (response.status === "OK") {
                    let options = [];
                    let options_raw = response.data[0];
                    for (let i in options_raw) {
                        if (options_raw.hasOwnProperty(i)) {
                            let option = options_raw[i];
                            options.push({key: option.toLowerCase(), value: option});
                        }
                    }
                    if (that.state.options == null) {
                        console.log(response.data);
                        that.setOptions(options);
                    }
                }
            }
        })
    }

    getData() {
        let that = this;
        $.ajax({
            url: App.getApiLink("channel/find/name/" + that.state.slug),
            method: "POST",
            contentType: "application/json",
            success: function (response) {
                if (response.status === "OK") {
                    let channel = response.data[0].channel;
                    console.log(channel);
                    let state = that.getStateProps(channel);
                    state.channel = channel;
                    that.setState(state);
                } else {
                    console.log("new");
                    let state = that.state;
                    state.is_new = true;
                    that.setState(state);
                }
            }
        })
    }

    sendData() {
        console.log(this.state);
        let that = this;
        let json = that.getStateProps(that.state);
        $.ajax({
            url: (that.state.is_new)? App.getApiLink("channel/create/") : App.getApiLink("channel/save/" + this.state.slug),
            method: (that.state.is_new)? "POST" : "PUT",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (response) {
                console.log(response);
                if (response.status === "OK") {
                    that.setButton("far fa-check", "success", "Saved changes");
                    MainRouter.refresh("/api/edit/" + that.state.type)
                } else {
                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            that.handleStatus(message.field, message.type, message.message);

                        }
                    }
                    that.setButton("far fa-times", "error", "Check the form");
                }
            },
            error: function (response) {
                console.log(response);
                that.setButton("far fa-times", "error", "Something went wrong...");
            }
        });
    }

    setOptions(options) {
        let state = this.state;
        state.options = options;
        this.setState(state);
    }

    handleStatus(field, type, message) {
        let state = this.state;
        state.status = (state.status == null)? {} : state.status;
        state.status[field] = {message: message, type: type};
        this.setState(state);
    }

    handleForm() {
        this.resetButton();
        let state = this.state;
        state.messages = this.resetMessages();
        this.setState(state);
        this.sendData();
    }

    // TODO: Implement toggle in list
    toggleOnline() {
        this.props.actions.onToggle(this.state.id, this.onToggle);
    }

    onToggle(channel) {
        console.log(channel);
        let state = this.getState(channel.id, channel.app_id, channel.secret, channel.type, channel.data_url, channel.active);
        state.channel = channel;
        this.setState(state);
    }

    render() {
        console.log(this.state);
        let actions = [];
        if (!this.state.is_new) {
            actions.push(<IconButton
                key={0}
                icon={`far fa-${(this.state.channel.token !== "")? 'shield-check' : 'shield'}`}
                class={`link ${this.state.channel.token !== ""? 'success' : ''}`}
                link={`/api/authorize/${this.state.type}`}/>
            );
            actions.push(<IconButton
                key={1}
                icon={`far fa-${this.state.active? "globe" : "signal-slash"}`}
                class={`link ${this.state.active? "success" : "error"}`}
                onClick={this.toggleOnline}/>
            );
            actions.push(<IconButton
                key={2}
                icon="far fa-trash-alt"
                link={`/api/delete/${this.state.type}`}
                class="link error"/>
            );
        }
        console.log(this.state.channel.name);
        return (
            <div className="box form-box">
                <form action="#" method="post" onSubmit={this.handleSubmit}>
                    <div className="box">
                        <div className="p-10 buttons d-flex justify-content-space-between">
                            <IconButton icon="far fa-arrow-left" link={"/api/management"} class="link"/>
                            <div className="buttons">
                                {actions}
                            </div>
                        </div>
                        <div className="p-20">
                            <Header
                                title={(this.state.is_new)? `New API Channel` : `Edit ${this.state.channel.name}`}
                                sub_title={this.state.channel.status.message}
                                sub_class={`text-${this.state.channel.status.type}`}/>
                            <Select
                                label={"Type"}
                                name={'type'}
                                value={this.state.type}
                                message={this.state.status.type.message}
                                class={this.state.status.type.type}
                                options={this.state.options}
                                onChange={this.handleInput}/>
                            <Input
                                label={"App/ Client id"}
                                name={'app_id'}
                                value={this.state.app_id}
                                message={this.state.status.app_id.message}
                                class={this.state.status.app_id.type}
                                onChange={this.handleInput}/>
                            <Input
                                label={"Secret id"}
                                name={'secret'}
                                value={this.state.secret}
                                message={this.state.status.secret.message}
                                class={this.state.status.secret.type}
                                onChange={this.handleInput}/>
                            <Input
                                label={"Data url"}
                                name={'data_url'}
                                value={this.state.data_url}
                                message={this.state.status.data_url.message}
                                class={this.state.status.data_url.type}
                                onChange={this.handleInput}/>
                            <div className="buttons pt-40 d-flex justify-content-space-between">
                                <div className="form">
                                    <Button
                                        icon={this.state.button.icon}
                                        text={this.state.button.text}
                                        class={this.state.button.type}
                                        ref={this.buttonSubmit}
                                        onClick={this.handleForm}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
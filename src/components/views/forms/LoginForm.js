import React from "react";
import $ from "jquery";
import {Header} from "../../Header";
import {Input} from "../../elements/form/Input";
import {Button} from "../../elements/form/Button";
import {MainRouter} from "../../../MainRouter";
import {App} from "../../../js/App";
import {FormData} from "../../../js/FormData";
import {ButtonLink} from "../../elements/form/ButtonLink";
import {Form} from "../../elements/form/Form";

export class LoginForm extends Form {

    componentDidMount() {
        this.setMessage("email", "default", "Enter your email", true);
        this.setMessage("password", "default", "Enter your password", true);
        this.setButton("lock", "Log in", "primary", true);
    }

    onSubmit() {
        let that = this;
        let email = (this.state.email != null)? this.state.email : null;
        let password = (this.state.password != null)? this.state.password : null;
        let error = false;
        this.resetMessages();

        if (!FormData.validateEmail(email)) {
            error = true;
            this.setMessage("email", "error", "Please use a valid email address");
        }
        if (email == null || email === "") {
            error = true;
            this.setMessage("email", "error", "E-mail can't be empty");
        }
        if (password == null || password === "") {
            error = true;
            this.setMessage("password", "error", "Password can't be empty");
        }
        if (!error) {
            setTimeout(function () {
                that.makeAPICall(email, password);
            }, 500);
            this.setProcessButton("Logging in...");
        }
    }

    makeAPICall(email, password) {
        let that = this;
        $.ajax({
            url: App.getApiLink("account/login/"),
            method: "post",
            contentType: "application/json",
            data: JSON.stringify({
                email: email,
                password: password
            }),
            success: function (response) {
                console.log(response);
                if (response.status === "OK") {
                    that.login(response.data[0].user.ID);
                } else {
                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            that.setMessage(message.field, message.type, message.message);
                        }
                    }
                    that.setButton("times", "Login failed", "error");
                }
            },
            error: function () {
                that.setMessage("password", "error", "Error while connecting to App...");
                that.setButton("times", "Login failed", 'error');
            }
        })
    }

    login(ID) {
        if (MainRouter.login(ID)) {
            this.setButton("check", "Login successful", "success");
            MainRouter.refresh("/");
        } else {
            this.setStatus("password", 'error', "Error while logging in...");
        }
    }

    render() {
        return (
            <form action="" method={"post"} onSubmit={this.handleSubmit}>
                <Header title={"Welcome to Hashlens"} sub_title={"Login"}/>
                <Input
                    key={"user-input"}
                    type={"text"}
                    name={"email"}
                    value={this.state.email}
                    label={"E-mail"}
                    message={this.getMessageText("email")}
                    class={this.getMessageType("email")}
                    ref={this.inputEmail}
                    onChange={this.handleInput}/>
                <Input
                    key={"password-input"}
                    type={"password"}
                    name={"password"}
                    value={this.state.password}
                    label={"Password"}
                    message={this.getMessageText("password")}
                    class={this.getMessageType("password")}
                    ref={this.inputPassword}
                    onChange={this.handleInput}/>
                <div className="row pt-30 d-flex justify-content-space-between">
                    <Button text={this.getButtonText()} icon={this.getButtonIcon()} class={this.getButtonType()}/>
                    <ButtonLink link={"/password-forget"} class={"primary link"} text={"Forgot password?"}/>
                </div>
                <div className="row pt-30">
                    <ButtonLink link={"/register"} class={"primary link"} text={"No account? Register now"}/>
                </div>
            </form>
        );
    }
}
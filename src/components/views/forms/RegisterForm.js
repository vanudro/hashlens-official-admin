import React from "react";
import $ from "jquery";
import {Header} from "../../Header";
import {Input} from "../../elements/form/Input";
import {Button} from "../../elements/form/Button";
import {App} from "../../../js/App";
import {FormData} from "../../../js/FormData";
import {ButtonLink} from "../../elements/form/ButtonLink";
import {Form} from "../../elements/form/Form";

export class RegisterForm extends Form {

    componentDidMount() {
        this.setMessage("first_name", "default", "Checking first name", true);
        this.setMessage("last_name", "default", "Checking last name", true);
        this.setMessage("email", "default", "Checking e-mail", true);
        this.setMessage("password", "default", "Checking password", true);
        this.setMessage("password_retype", "default", "Checking retype", true);
        this.setButton("paper-plane", "Register", "primary", true);
    }

    onSubmit() {
        let that = this;
        let first_name = (this.state.first_name != null)? this.state.first_name : null;
        let last_name = (this.state.last_name != null)? this.state.last_name : null;
        let email = (this.state.email != null)? this.state.email : null;
        let password = (this.state.password != null)? this.state.password : null;
        let password_retype = (this.state.password_retype != null)? this.state.password_retype : null;

        this.resetMessages();
        this.setProcessButton("Checking");
        if (this.checkForm(first_name, last_name, email, password, password_retype)) {
            setTimeout(function () {
                that.makeAPICall(first_name, last_name, email, password);
            }, 500);
        } else {
            that.setButton("times", "Check form", 'error');
        }
    }

    checkForm(first_name, last_name, email, password, password_retype) {
        let error = false;
        if (first_name == null || first_name === "") {
            this.setMessage("first_name", 'error', "First name can't be empty");
            error = true;
        }
        if (last_name == null || last_name === "") {
            this.setMessage("last_name", 'error', "Last name can't be empty");
            error = true;
        }
        if (!FormData.validateEmail(email)) {
            this.setMessage("email", 'error', "Please use a valid email address");
            error = true;
        }
        if (email == null || email === "") {
            this.setMessage("email", 'error', "E-mail can't be empty");
            error = true;
        }
        if (password == null || password === "") {
            this.setMessage("password", 'error', "Password can't be empty");
            error = true;
        }
        if (password_retype !== password) {
            this.setMessage("password", 'error', "Passwords don't match");
            this.setMessage("password_retype", 'error', "Passwords don't match");
            error = true;
        }
        return !error;
    }

    makeAPICall(first_name, last_name, email, password) {
        let that = this;
        $.ajax({
            url: App.getApiLink("account/register/"),
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                email: email,
                first_name: first_name,
                last_name: last_name,
                password: password,
                link: App.getLink("activation")
            }),
            success: function (response) {
                if (response.status === "OK") {
                    that.setButton("check", "Registration successful", "success");
                } else {
                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            that.setMessage(message.field, message.type, message.message);
                        }
                    }
                    that.setButton("times", "Registration failed", 'error');
                }
            },
            error: function () {
                that.setButton("times", "Registration failed", 'error');
            }
        })
    }

    render() {
        return (
            <form action="" method={"post"} onSubmit={this.handleSubmit}>
                <Header title={"Welcome to Hashlens"} sub_title={"Register your new account"}/>
                <Input
                    name={"first_name"}
                    value={this.state.first_name}
                    label={"First name"}
                    message={this.getMessageText("first_name")}
                    class={this.getMessageType("first_name")}
                    onChange={this.handleInput}/>
                <Input
                    name={"last_name"}
                    value={this.state.last_name}
                    label={"Last name"}
                    message={this.getMessageText("last_name")}
                    class={this.getMessageType("last_name")}
                    onChange={this.handleInput}/>
                <Input
                    name={"email"}
                    value={this.state.email}
                    label={"E-mail"}
                    message={this.getMessageText("email")}
                    class={this.getMessageType("email")}
                    onChange={this.handleInput}/>
                <Input
                    type={"password"}
                    name={"password"}
                    value={this.state.password}
                    label={"New password"}
                    message={this.getMessageText("password")}
                    class={this.getMessageType("password")}
                    onChange={this.handleInput}/>
                <Input
                    type={"password"}
                    name={"password_retype"}
                    value={this.state.password_retype}
                    label={"Retype password"}
                    message={this.getMessageText("password_retype")}
                    class={this.getMessageType("password_retype")}
                    onChange={this.handleInput}/>
                <div className="row pt-30 d-flex justify-content-space-between">
                    <Button
                        text={this.getButtonText()}
                        icon={this.getButtonIcon()}
                        class={this.getButtonType()}/>
                    <ButtonLink link={"/login"} class={"primary link"} text={"Back to login"}/>
                </div>
            </form>
        );
    }
}
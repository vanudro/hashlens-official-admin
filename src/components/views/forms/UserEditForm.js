import React from "react";
import $ from "jquery";
import {Header} from "../../Header";
import {Input} from "../../elements/form/Input";
import {Button} from "../../elements/form/Button";
import {App} from "../../../js/App";
import {MainRouter} from "../../../MainRouter";
import {FormData} from "../../../js/FormData";
import {Avatar} from "../../Avatar";
import {IconButton} from "../../elements/form/IconButton";
import {Form} from "../../elements/form/Form";

export class UserEditForm extends Form {

    getState(first_name, last_name, email, image) {
        return {first_name: first_name, last_name: last_name, email: email, image: `${image}?s=1000`};
    }

    componentDidMount() {
        this.setMessage("first_name", "default", "Enter your first name", true);
        this.setMessage("last_name", "default", "Enter your last name", true);
        this.setMessage("email", "default", "Enter your email address", true);
        this.setButton("save", "Save", "primary", true);

        let that = this;
        $.ajax({
            url: App.getApiLink(`user/find/${MainRouter.getID()}`),
            method: "POST",
            contentType: "application/json",
            success: function (response) {
                console.log(response);
                let user = response.data[0].user;
                that.setState(that.getState(user.first_name, user.last_name, user.email, user.image))
            }
        });
    }

    onSubmit() {
        let that = this;
        console.log(this.state);
        let first_name = (this.state.first_name != null)? this.state.first_name : null;
        let last_name = (this.state.last_name != null)? this.state.last_name : null;
        let email = (this.state.email != null)? this.state.email : null;
        this.resetMessages();

        setTimeout(function () {
            if (that.checkForm(first_name, last_name, email)) {
                that.updateUser(first_name, last_name, email);
                console.log("All good");
            } else {
                that.setButton("times", "Failed...", "error");
            }
        }, 500);
        this.setProcessButton("Saving");
    }

    checkForm(first_name, last_name, email) {
        if (first_name == null || first_name === "") {
            this.setMessage("first_name", "error", "Please enter your first name.");
            return false;
        }
        if (last_name == null || last_name === "") {
            this.setMessage("last_name", "error", "Please enter your last name.");
            return false;
        }
        if (email == null || email === "") {
            this.setMessage("email", "error", "Please enter your e-mail.");
            return false;
        }
        if (!FormData.validateEmail(email)) {
            this.setMessage("email", "error", "Please enter a valid e-mail address.");
            return false;
        }
        return true;
    }

    updateUser(first_name, last_name, email) {
        let that = this;
        $.ajax({
            url: App.getApiLink(`user/update/${MainRouter.getID()}`),
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify({
                first_name: first_name,
                last_name: last_name,
                email: email
            }),
            success: function (response) {
                console.log("Whohoo");
                console.log(response);

                if (response.status === "OK") {
                    that.setButton("check", "Saved!", "success");
                } else {
                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            that.setMessage(message.field, message.type, message.message);
                        }
                    }
                }
            },
            error: function (response) {
                that.setButton("times", "Failed...", "error");
            }
        });
    }

    render() {
        console.log(this.state);
        return (
            <div>
                <Avatar image={this.state.image} alt={`${this.state.first_name} ${this.state.last_name}`}/>
                <div className="buttons top-bar d-flex justify-content-space-between p-10">
                    <IconButton icon="far fa-arrow-left" link={"/dashboard"} class="link"/>
                    <div className="actions">
                        <IconButton icon="far fa-key" class="link" link={"/account/password"}/>
                        <IconButton icon="far fa-trash-alt" class="error link" link={"/account/delete/"}/>
                    </div>
                </div>
                <form action="#" method="post" onSubmit={this.handleSubmit}>
                    <div className="px-20 pb-20 pt-50">
                        <Header
                            title={`${this.state.first_name} ${this.state.last_name}`} sub_title={this.state.email}/>
                        <Input
                            label={"First name"}
                            name={'first_name'}
                            value={this.state.first_name}
                            message={this.getMessageText("first_name")}
                            class={this.getMessageType("first_name")}
                            onChange={this.handleInput}/>
                        <Input
                            label={"Last name"}
                            name={'last_name'}
                            value={this.state.last_name}
                            class={this.getMessageType("last_name")}
                            message={this.getMessageText("last_name")}
                            ref={this.inputLastName}
                            onChange={this.handleInput}/>
                        <Input
                            label={"E-mail"}
                            name={'email'}
                            value={this.state.email}
                            class={this.getMessageType("email")}
                            message={this.getMessageText("email")}
                            ref={this.inputEmail}
                            onChange={this.handleInput}/>
                        <div className="buttons pt-40 d-flex justify-content-space-between">
                            <div className="form">
                                <Button
                                    icon={this.getButtonIcon()}
                                    text={this.getButtonText()}
                                    class={this.getButtonType()}/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
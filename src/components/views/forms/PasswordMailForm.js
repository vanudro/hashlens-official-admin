import React from "react";
import $ from 'jquery';
import {Header} from "../../Header";
import {Button} from "../../elements/form/Button";
import {FormData} from "../../../js/FormData";
import {Input} from "../../elements/form/Input";
import {App} from "../../../js/App";
import {ButtonLink} from "../../elements/form/ButtonLink";
import {Form} from "../../elements/form/Form";

export class PasswordMailForm extends Form {

    componentDidMount() {
        this.setMessage("email", "default", "Enter your e-mail address", true);
        this.setButton("paper-plane", "Send", "primary", true)
    }

    onSubmit() {
        let email = (this.state.email != null)? this.state.email : null;
        this.setProcessButton("Sending");

        if (this.checkForm(email)) {
            this.sendEmail(email);
        } else {
            this.setButton("times", "Failed", "error");
        }
    }

    checkForm(email) {
        if (email == null || email === "") {
            this.setMessage("email", "error", "Please enter your e-mail address.");
            return false
        }
        if (!FormData.validateEmail(email)) {
            this.setMessage("email", "error", "Please enter a valid e-mail address.");
            return false
        }
        return true;
    }

    sendEmail(email) {
        let that = this;
        $.ajax({
            url: App.getApiLink("account/password/forgot"),
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                email: email,
                link: App.getLink("password-recover")
            }),
            success: function (response) {
                if (response.status === "OK") {
                    that.setButton("envelope", "Check your mail", "success");
                    setTimeout(function () {
                        that.setMessage("email", "info", "No mail? Click the button below.");
                        that.setButton("paper-plane", "Send again", "info");
                    }, 5000)
                } else {
                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            that.setMessage("email", message.type, message.message);
                        }
                    }
                    that.setButton("times", "Failed", "error");
                }
            },
            error: function (response) {
                that.setMessage("email", "error", response.message);
                that.setButton("times", "Failed", "error");
            }
        })
    }

    render() {
        return (
            <form action="" method={"post"} onSubmit={this.handleSubmit}>
                <Header title={"Forgot password?"} sub_title={"No worries, We can send you a new one."}/>
                <div className="row pt-20">
                    <Input
                        name={"email"}
                        value={this.state.email}
                        className={"input"}
                        label={"E-mail"}
                        message={this.getMessageText("email")}
                        class={this.getMessageType("email")}
                        onChange={this.handleInput}/>
                </div>
                <div className="row pt-30 d-flex justify-content-space-between">
                    <Button
                        text={this.getButtonText()}
                        icon={this.getButtonIcon()}
                        class={this.getButtonType()}
                    />
                    <ButtonLink link={"/login"} class={"primary link"} text={"Back to Login"}/>
                </div>
            </form>
        );
    }

}
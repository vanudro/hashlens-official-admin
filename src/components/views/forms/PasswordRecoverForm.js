import React from "react";
import $ from "jquery";
import {Header} from "../../Header";
import {Input} from "../../elements/form/Input";
import {Button} from "../../elements/form/Button";
import {App} from "../../../js/App";
import {MainRouter} from "../../../MainRouter";
import {Form} from "../../elements/form/Form";

export class PasswordRecoverForm extends Form {

    getState(new_password1, new_password2, hash) {
        return {new_password1: new_password1, new_password2: new_password2, hash: hash}
    }

    componentDidMount() {
        this.setMessage("password", "default", "Enter a new password.");
        this.setMessage("password", "default", "Retype your new password.");
        this.setButton("save", "Change", "primary");
    }

    onSubmit() {
        console.log(this.state);
        let that = this;
        let new_password1 = (this.state.new_password1 != null)? this.state.new_password1 : null;
        let new_password2 = (this.state.new_password2 != null)? this.state.new_password2 : null;

        this.inputNewPassword1.current.setStatus("", "Enter a new password.");
        this.inputNewPassword2.current.setStatus("", "Retype your new password.");
        this.buttonSubmit.current.setButton("Saving", "primary", "far fa-spin fa-sync");

        setTimeout(function () {
            if (that.validateForm(new_password1, new_password2)) {
                that.updatePassword(new_password1);
            } else {
                that.buttonSubmit.current.setButton("Failed...", "error", "far fa-times");
            }
        }, 500);

    }

    validateForm(new_password1, new_password2) {
        if (new_password1 == null || new_password1 === "") {
            this.inputNewPassword1.current.setStatus("error", "Please enter a new password.");
            return false;
        }
        if (new_password2 == null || new_password2 === "") {
            this.inputNewPassword2.current.setStatus("error", "Retype your new password.");
            return false;
        }
        if (new_password1 !== new_password2) {
            this.inputNewPassword1.current.setStatus("error", "New passwords aren't the same.");
            this.inputNewPassword2.current.setStatus("error", "New passwords aren't the same.");
            return false;
        }
        return true;
    }

    updatePassword(new_password) {
        let that = this;

        let url = new URL(window.location.href);
        let hash = url.searchParams.get("token");
        hash = (hash != null && hash !== "")? hash : "none";

        $.ajax({
            url: App.getApiLink(`account/password/new/${hash}`),
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify({
                password: new_password,
            }),
            success: function (response) {
                console.log("Whohoo");
                console.log(response);

                if (response.status === "OK") {
                    that.buttonSubmit.current.setButton("Changed password!", "success", "far fa-check");
                    MainRouter.refresh("../login", 3000);
                } else {

                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            console.log(response.field);
                            that.inputNewPassword1.current.setStatus(message.type, message.message);
                        }
                    }

                    that.buttonSubmit.current.setButton("Failed...", "error", "far fa-times");
                }
            },
            error: function (response) {
                console.log(response);
                that.buttonSubmit.current.setButton("Saving failed...", "error", "far fa-times");
            }
        });
    }

    renderMessage() {
        return (
            <div>
                <Header title={"Something went wrong"} sub_title={"Please go back to the login page"}/>
                <div className="pt-20">
                    <Button link={"../login"} text={"Back to login"} class="primary link"/>
                </div>
            </div>
        );
    }

    renderForm() {
        return (
            <form action="#" method="post" onSubmit={this.handleSubmit}>
                <div className="box p-20">
                    <Header
                        title={`New password`} sub_title={"Think of something you can remember"}/>
                    <Input
                        label={"New password"}
                        type={"password"}
                        name={'new_password1'}
                        value={this.state.new_password1}
                        message={"Enter a new password."}
                        onChange={this.handleInput}/>
                    <Input
                        label={"Retype"}
                        type={"password"}
                        name={'new_password2'}
                        value={this.state.new_password2}
                        message={"Retype your new password."}
                        onChange={this.handleInput}/>
                    <div className="buttons pt-40 d-flex justify-content-space-between">
                        <div className="form">
                            <Button icon={"far fa-save"} text={"Save"}/>
                        </div>
                    </div>
                </div>
            </form>
        );
    }

    render() {
        if (this.state.hash !== "none") {
            return this.renderForm();
        } else {
            return this.renderMessage();
        }
    }

}
import React from "react";
import $ from "jquery";
import {Button} from "../../elements/form/Button";
import {Header} from "../../Header";
import {Input} from "../../elements/form/Input";
import {App} from "../../../js/App";
import {MainRouter} from "../../../MainRouter";
import {Avatar} from "../../Avatar";
import {ButtonLink} from "../../elements/form/ButtonLink";
import {IconButton} from "../../elements/form/IconButton";
import {Form} from "../../elements/form/Form";

export class UserDeleteForm extends Form {

    getState(first_name, last_name, email, image, password) {
        return {first_name: first_name, last_name: last_name, email: email, image: image, password: password};
    }

    componentDidMount() {
        this.setMessage("password", "default", "Enter your password to confirm account removal.", true);
        this.setButton("trash-alt", "Delete", "error", true);

        let that = this;
        $.ajax({
            url: App.getApiLink(`user/find/${MainRouter.getID()}`),
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                id: MainRouter.getID()
            }),
            success: function (response) {
                console.log(response);
                let user = response.data[0].user;
                that.setState(that.getState(user.first_name, user.last_name, user.email, `${user.image}?s=1000`))
            }
        });
    }

    onSubmit() {
        let that = this;
        let password = (this.state.password != null)? this.state.password : null;
        this.setButton("spin fa-sync", "Deleting...", "primary");

        setTimeout(function () {
            if (password == null || password === "") {
                that.setMessage("password", "error", "Please enter your current password.");
                that.setButton("far fa-times", "Failed...", "error");
            } else {
                that.deleteUser(password);
            }
        }, 500);

    }

    deleteUser(password) {
        let that = this;
        $.ajax({
            url: App.getApiLink(`user/delete/${MainRouter.getID()}`),
            method: "DELETE",
            contentType: "application/json",
            data: JSON.stringify({
                password: password
            }),
            success: function (response) {
                if (response.status === "OK") {
                    // that.header.current.setHeader("Account deleted", "Sorry to see you go...");
                    that.setButton("check", "Delete complete", "info");
                    setTimeout(function () {
                        that.setButton("spin fa-sync", "Logging out...", "primary");
                        MainRouter.logout(true);
                        MainRouter.refresh("/logout", 1000);
                    }, 1000);
                } else {
                    for (let i in response.messages) {
                        if (response.messages.hasOwnProperty(i)) {
                            let message = response.messages[i];
                            that.setMessage(message.field, message.type, message.message);
                        }
                    }
                    that.setButton("times", "Failed...", "error");
                }
            },
            error: function () {
                that.setButton("times", "Deleting failed...", "error");
            }
        });
    }

    render() {
        return (
            <form action="#" method="post" onSubmit={this.handleSubmit}>
                <div className="box">
                    <Avatar image={this.state.image}/>
                    <div className="buttons top-bar d-flex justify-content-space-between p-10">
                        <IconButton icon="far fa-arrow-left" class="link" link={"/account"}/>
                    </div>
                    <div className="pt-50">
                        <div className="p-20">
                            <Header
                                title={"Delete your account"}
                                sub_title={`Hey eh.. ${this.state.first_name}... are you sure about this?`}
                                ref={this.header}/>
                            <Input
                                label={"Current password"}
                                type={"password"}
                                name={'password'}
                                value={this.state.password}
                                message={this.getMessageText("password")}
                                class={this.getMessageType("password")}
                                onChange={this.handleInput}/>
                            <div className="buttons big pt-40 d-flex justify-content-space-between">
                                <Button
                                    text={this.getButtonText()}
                                    icon={this.getButtonIcon()}
                                    class={this.getButtonType()}/>
                                <ButtonLink text={"Cancel"} link={"/account"} class="default"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }

}
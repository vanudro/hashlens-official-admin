import React from "react";
import $ from 'jquery';
import {ChannelRowBox} from "../elements/dashboard/ChannelRowBox";
import {App} from "../../js/App";
import {IconButton} from "../elements/form/IconButton";

export class ChannelListView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {rows: []};
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        let that = this;
        $.ajax({
            url: App.getApiLink("channel/get/list"),
            method: "POST",
            contentType: "application/json",
            success: function (response) {
                console.log(response.data);
                if (response.status === "OK") {
                    let rows = [];
                    for (let i in response.data) {
                        if (response.data.hasOwnProperty(i)) {
                            let channel = response.data[i].channel;
                            rows.push(<ChannelRowBox key={i} channel={channel} onToggle={that.props.actions.onToggle}/>);

                        }
                    }
                    rows.push(
                        <div id="add-api" className="api-box box d-flex justify-content-center align-items-center py-10">
                            <IconButton icon={"far fa-plus"} link={"/api/add"} class={"primary link"}/>
                        </div>
                    );
                    that.setState({rows: rows})
                }
            },
            error: function (response) {
                that.setState({rows: (<h1>Nothing found...</h1>)})
            }
        })
    }


    render() {
        return (
            <div className="container wrapper h-100">
                <h1>API Management</h1>
                <div className="pt-20">
                    {this.state.rows}
                </div>
            </div>
        );
    }
}